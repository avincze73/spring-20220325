package hu.sprintacademy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(JpaConfiguration.class);
        ProjectService projectService =
                applicationContext.getBean(ProjectService.class);

        Employee employee = new Employee();
        employee.setId(1L);
        employee.setLastName("lastName1");
        employee.setFirstName("lastName1");
        employee.setPassword("password1");
        employee.setUserName("userName1");
        employee.setAddress(new Address("1111", "Budapest", "Ablak u. 12."));

        Project project = new Project();
        project.setName("project1");
        project.setProjectPeriod(new ProjectPeriod(new Date(), new Date(new Date().getTime()-100000)));

        projectService.addProject(project, employee);
    }
}
