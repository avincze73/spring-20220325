package hu.sprintacademy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;


    @Transactional
    public void save(Project project) {
        entityManager.persist(entityManager.merge(project));
    }
}
