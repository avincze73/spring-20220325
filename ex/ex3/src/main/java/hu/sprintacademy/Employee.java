package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Employee extends NNEntity {
	@Basic(optional = false)
	@Column(name = "firstName")
	private String firstName;
	@Basic(optional = false)
	@Column(name = "lastName")
	private String lastName;
	@Basic(optional = false)
	@Column(name = "userName")
	private String userName;
	@Basic(optional = false)
	@Column(name = "password")
	private String password;
	@Embedded
	private Address address;

	@ManyToOne(optional = true)
	@JoinColumn(name = "bossId", referencedColumnName = "id")
	private Employee boss;

	@ManyToOne(optional = true)
	@JoinColumn(name = "departmentId", referencedColumnName = "id")
	private Department department;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "employeeRoles", 
	joinColumns = @JoinColumn(name = "employeeId", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "roleId", referencedColumnName = "id"))
	private Set<Role> roleList;

}
