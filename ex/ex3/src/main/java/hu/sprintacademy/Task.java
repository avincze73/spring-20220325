package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Task extends NNEntity {
	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@ManyToOne(optional = false)
	@JoinColumn(name = "participantId", referencedColumnName = "id")
	private Employee participant;

	
	public Task() {
		// TODO Auto-generated constructor stub
	}
	
	public Task(String name, Employee participant) {
		super();
		this.name = name;
		this.participant = participant;
	}
}
