package hu.sprintacademy;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Department extends NNEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@Embedded
	private Address address;

	public Department() {
		// TODO Auto-generated constructor stub
		this(null, null);
	}

	public Department(String name, Address address) {
		super();
		this.name = name;
		this.address = address;
	}

}
