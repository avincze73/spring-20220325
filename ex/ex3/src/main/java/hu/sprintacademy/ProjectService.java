package hu.sprintacademy;

import org.hibernate.annotations.ValueGenerationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService {

    private final EmployeeRepository employeeRepository;

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository, EmployeeRepository employeeRepository) {
        this.projectRepository = projectRepository;
        this.employeeRepository = employeeRepository;
    }

    @Transactional
    public void addProject(Project project, Employee employee) {

        employeeRepository.save(employee);
        project.setOwner(employee);
        projectRepository.save(project);

    }
}
