package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Project extends NNEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@Embedded
	private ProjectPeriod projectPeriod;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ownerId", referencedColumnName = "id")
	private Employee owner;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "projectId", referencedColumnName = "id")
	private List<Task> taskList;

}
