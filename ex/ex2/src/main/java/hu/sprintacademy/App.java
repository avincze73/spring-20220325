package hu.sprintacademy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(EmployeeConfiguration.class);
        EmployeeService employeeService =
                applicationContext.getBean(EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employeeService.add(employee);
        employeeService.findAll().forEach(System.out::println);
    }
}
