package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Setter
@Getter
public class EmployeeService {

    //@Qualifier("inMemoryEmployeeRepository")
    private final EmployeeRepository employeeRepository;

    public EmployeeService(@Qualifier("inMemoryEmployeeRepository") EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

    public void add(Employee employee){
        employeeRepository.add(employee);
    }
}
