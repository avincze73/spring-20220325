package hu.sprintacademy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@Configuration
@ComponentScan(basePackages = "hu.sprintacademy")
public class EmployeeConfiguration {
    
    @Bean
    public Employee employee1(){
        return new Employee(1, "firstName1", "lastName1", "loginName1",
                "password1", "title1", 1000);
    }

    @Bean
    public Employee employee2(){
        return new Employee(2, "firstName2", "lastName2", "loginName2",
                "password2", "title2", 2000);
    }

    @Bean
    public Employee employee3(){
        return new Employee(3, "firstName3", "lastName3", "loginName3",
                "password3", "title3", 3000);
    }

    @Bean
    public Employee employee4(){
        return new Employee(4, "firstName4", "lastName4", "loginName4",
                "password4", "title4", 4000);
    }

    @Bean
    public Employee employee5(){
        return new Employee(5, "firstName5", "lastName5", "loginName5",
                "password5", "title5", 5000);
    }

    @Bean
    public EmployeeRepository inMemoryEmployeeRepository(){
        InMemoryEmployeeRepository repository = new InMemoryEmployeeRepository();
        repository.setDatabase(new ArrayList<>());
        repository.add(employee1());
        repository.add(employee2());
        repository.add(employee3());
        repository.add(employee4());
        repository.add(employee5());
        return repository;
    }

    @Bean
    public EmployeeRepository jdbcEmployeeRepository(){
        return new JdbcEmployeeRepository();
    }

//    @Bean
//    public EmployeeService employeeService(){
//        EmployeeService employeeService = new EmployeeService();
//        employeeService.setInMemoryEmployeeRepository(inMemoryEmployeeRepository());
//        return employeeService;
//    }
    
    
}
