package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class InMemoryEmployeeRepository implements EmployeeRepository{

    private List<Employee> database;

    @Override
    public void add(Employee employee) {
        database.add(employee);
    }

    @Override
    public List<Employee> findAll() {
        return new ArrayList<>(database);
    }
}
