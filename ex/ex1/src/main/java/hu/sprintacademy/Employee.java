package hu.sprintacademy;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee {

    private int id;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private String title;
    private int salary;

}
