package hu.sprintacademy;

import java.util.List;

public interface EmployeeRepository {
    void add(Employee employee);
    List<Employee> findAll();
}
