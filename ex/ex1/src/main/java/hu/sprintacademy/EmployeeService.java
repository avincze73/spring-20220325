package hu.sprintacademy;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeService {

    private EmployeeRepository inMemoryEmployeeRepository;

    public List<Employee> findAll(){
        return inMemoryEmployeeRepository.findAll();
    }

    public void add(Employee employee){
        inMemoryEmployeeRepository.add(employee);
    }
}
