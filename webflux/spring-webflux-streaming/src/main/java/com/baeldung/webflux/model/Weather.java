package com.baeldung.webflux.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Weather {

    private String temperature;
    private String humidity;
    private String windSpeed;

}
