package com.baeldung.webflux.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class WeatherEvent {

    private static int COUNTER = 0;
    private int index;
    private Weather weather;
    private LocalDateTime date;


    public WeatherEvent(Weather weather, LocalDateTime date) {
        this.weather = weather;
        this.date = date;
        index = ++COUNTER;
    }
}
