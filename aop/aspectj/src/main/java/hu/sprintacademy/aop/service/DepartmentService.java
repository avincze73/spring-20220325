package hu.sprintacademy.aop.service;

import hu.sprintacademy.aop.aspect.LogExecutionBefore;
import hu.sprintacademy.aop.aspect.LogExecutionTime;
import hu.sprintacademy.aop.model.Department;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    @LogExecutionBefore
    public void add(Department department){

    }


    @LogExecutionBefore
    public Department getById(Integer id){
        return new Department(1, "name1");
    }

    @LogExecutionTime
    //@LogExecutionBefore
    public String getNameById(Integer id){
        return "";
    }
}
