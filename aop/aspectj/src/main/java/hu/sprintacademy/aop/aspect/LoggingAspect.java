package hu.sprintacademy.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.logging.Logger;


@Component
@Aspect
public class LoggingAspect {

    private static final Logger log = Logger.getLogger(LoggingAspect.class.getName());

    
    
    @Pointcut("execution(* hu.sprintacademy.aop.service.EmployeeService.getEmployee(..))") // the pointcut expression
    public void serviceMethod() { // the pointcut signature
    }


    @Pointcut("execution(* hu.sprintacademy.aop.model.Employee.getEmpName(..))") // the pointcut expression
    public void serviceMethod2() { // the pointcut signature
    }

//    @Pointcut("execution(* hu.sprintacademy.aop.service.EmployeeService.addEmployee(..))") // the pointcut expression
//    public void serviceMethod3() { // the pointcut signature
//    }




    @Before("serviceMethod()")
    public void logBefore(JoinPoint joinPoint) {
        log.info("logBefore() is running on " + joinPoint.getSignature().getName());
    }

    //@Before("serviceMethod2()")
    public void logBefore2(JoinPoint joinPoint) {
        log.info("logBefore() is running on " + joinPoint.getSignature().getName());
    }

    @After("serviceMethod()")
    public void logAfter(JoinPoint joinPoint) {
        log.info("logAfter() is running on " + joinPoint.getSignature().getName());
    }

    @AfterReturning(pointcut = "serviceMethod2()", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        log.info("logAfterReturning() is running!");
        log.info("hijacked : " + joinPoint.getSignature());
        log.info("Method returned value is : " + result);
        log.info("******");
    }

    //@Around("serviceMethod3()")
    @Around("execution(* hu.sprintacademy.aop.service.EmployeeService.addEmployee(..))")
    //@Pointcut("execution(* hu.sprintacademy.aop.service.EmployeeService.addEmployee(..))") // the pointcut expression
    public void logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("logAround() is running!");
        System.out.println("hijacked method : " + joinPoint.getSignature().getName());
        System.out.println("hijacked arguments : " + Arrays.toString(joinPoint.getArgs()));

        System.out.println("Around before is running!");
        joinPoint.proceed(); //continue on the intercepted method
        System.out.println("Around after is running!");

        System.out.println("******");
    }

    @AfterThrowing(
            pointcut = "execution(* hu.sprintacademy.aop.service.EmployeeService.throwException(..))",
            throwing = "error")
   public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
        log.info("logAfterThrowing() is running!");
        log.info("hijacked : " + joinPoint.getSignature().getName());
        log.info("Exception : " + error);
        log.info("******");
    }
}
