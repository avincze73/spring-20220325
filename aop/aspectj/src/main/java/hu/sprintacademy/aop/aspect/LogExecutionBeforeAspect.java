package hu.sprintacademy.aop.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Aspect
@Configuration
public class LogExecutionBeforeAspect {

    private static final Logger logger = Logger.getLogger(LogExecutionBeforeAspect.class.getName());


    @Before("@annotation(hu.sprintacademy.aop.aspect.LogExecutionBefore)")
    public void logExecutionTimeOfMethodCall(JoinPoint joinPoint) throws Throwable {
        logger.info("LogExecutionBefore logBefore() is running " + joinPoint.getSignature().getName());
    }

}
