package hu.sprintacademy;

import hu.sprintacademy.aop.AopConfig;
import hu.sprintacademy.aop.model.Department;
import hu.sprintacademy.aop.model.Employee;
import hu.sprintacademy.aop.service.DepartmentService;
import hu.sprintacademy.aop.service.EmployeeService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AopConfig.class);
        EmployeeService employeeService =
                applicationContext.getBean("employeeService", EmployeeService.class);
        Employee employee = employeeService.getEmployee();
        String name = employee.getEmpName();
        //System.out.println(employee.getEmpName());
        try {
            employeeService.throwException();
        } catch (Exception e) {
            //e.printStackTrace();
        }
//
        //employeeService.addEmployee("András");
//
        DepartmentService departmentService = applicationContext.getBean(DepartmentService.class);
        departmentService.add(new Department());
        departmentService.getById(1);
        departmentService.getNameById(1);

    }
}
