package com.sprint.spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

@SpringBootApplication
@EnableEurekaClient

//user1:5f4dcc3b5aa765d61d8327deb882cf99
public class AuthenticationService3Application {

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationService3Application.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository) {
        return (args) -> {
            Account a1 = new Account();
            a1.setUsername("user1");
            a1.setPassword(new Md5PasswordEncoder().encodePassword("password", null));
            a1.setFullname("user 1");
            accountRepository.save(a1);

            a1 = new Account();
            a1.setUsername("user2");
            a1.setPassword(new Md5PasswordEncoder().encodePassword("password", null));
            a1.setFullname("user 2");
            accountRepository.save(a1);
        };

    }

}
