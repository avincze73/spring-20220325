package hu.cib.web.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class WrongController {

    @RequestMapping({"/wrong"})
    public String wrong(Model model) {
        return "/wrong";
    }
}
