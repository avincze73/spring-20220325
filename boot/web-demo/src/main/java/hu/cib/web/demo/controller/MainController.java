package hu.cib.web.demo.controller;

import hu.cib.web.demo.properties.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class MainController {

    @Autowired
    private SprintAcademyProperties sprintAcademyProperties;

    @Autowired
    private ExternalItem externalItem;

    @Autowired
    private PropertyConversion propertyConversion;

    @Autowired
    private ImmutableCredentials immutableCredentials;

    @Autowired
    private PropertiesWithJavaConfig propertiesWithJavaConfig;

    @Autowired
    private RandomProperties randomProperties;




    @RequestMapping({"/", "/main"})
    public String main(Model model) {
        model.addAttribute("greeting", "Hello from controller");
        model.addAttribute("host", sprintAcademyProperties.getHostname());
        model.addAttribute("defaultRecipients", sprintAcademyProperties.getDefaultRecipients().get(0));
        model.addAttribute("additionalHeaders", sprintAcademyProperties.getAdditionalHeaders().get("secure"));
        model.addAttribute("credentials", sprintAcademyProperties.getCredentials().getUsername());
        model.addAttribute("externalitem", externalItem.getName());
        model.addAttribute("propertyConversion", propertyConversion.getTimeInDays());
        model.addAttribute("datasize", propertyConversion.getSizeInGB());
        model.addAttribute("employee", propertyConversion.getEmployee().getSalary());
        model.addAttribute("immutableCredentials", immutableCredentials.getPassword());
        model.addAttribute("version", sprintAcademyProperties.getVersion());
        model.addAttribute("fooname", propertiesWithJavaConfig.getFooName());
        model.addAttribute("barname", propertiesWithJavaConfig.getBarName());
        model.addAttribute("bardefault", propertiesWithJavaConfig.getBardefault());
        model.addAttribute("random", randomProperties.getMyInt());


        return "/main";
    }


}
