package hu.cib.web.demo.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "mail.credentials")
@ConstructorBinding
@AllArgsConstructor
@Getter
public class ImmutableCredentials {

    private final String authMethod;
    private final String username;
    private final String password;
}
