package hu.cib.web.demo.properties;

import hu.cib.web.demo.servlet.SprintAcademyServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SprintAcademyServletConfig {


    @Bean
    public ServletRegistrationBean exampleServletBean() {
        ServletRegistrationBean bean = new ServletRegistrationBean(
                new SprintAcademyServlet(), "/sprintacademyservlet/*");
        bean.setLoadOnStartup(1);
        return bean;
    }
}
