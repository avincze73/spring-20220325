package hu.cib.web.demo.controller;

import hu.cib.web.demo.properties.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class SimpleController {

    @Autowired
    private SprintAcademyProperties iqjbProperties;

    @RequestMapping({"/simple"})
    public String main(Model model) {
        model.addAttribute("greeting", "Hello from controller");
        model.addAttribute("host", iqjbProperties.getHostname());
        return "/simple";
    }
}
