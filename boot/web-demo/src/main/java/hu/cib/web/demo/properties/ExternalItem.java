package hu.cib.web.demo.properties;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalItem {

    private String name;
    private int size;
}
