package hu.sprint.hateoas.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Slf4j
@RequestMapping(value = "/customers")
public class CustomerController {

    @GetMapping("/{customerId}")
    public Customer getCustomerById(@PathVariable String customerId) {
        Customer customer = new Customer(customerId, "name1", "company1", null);

        Link selfLink = linkTo(CustomerController.class).slash(customer.getCustomerId()).withSelfRel();
        customer.add(selfLink);
        return customer;
    }

}
