# Web Application

## Deploying to kubernetes
```bash
docker-compose build
docker-compose push


kubectl apply -f ts-ems/mysql-data-persistentvolumeclaim.yaml
kubectl apply -f ts-ems/db-deployment.yaml
kubectl apply -f ts-ems/db-service.yaml
kubectl apply -f ts-ems/ts-ems-deployment.yaml
kubectl apply -f ts-ems/ts-ems-service.yaml
kubectl logs ts-ems-f7d67564c-qm9nm
```


 ## Using configmap
 ```bash
kubectl create configmap ts-ems-config --from-literal=EMS_DB=ts
kubectl get configmap ts-ems-config
kubectl describe configmap/ts-ems-config
kubectl edit configmap/ts-ems-config
kubectl scale deployment ts-ems --replicas=0
kubectl scale deployment ts-ems --replicas=1
kubectl apply -f ts-ems-deployment-2.yaml
kubectl logs ts-ems-5ffc4cf754-5fpxf -f
 ```

 ## Using secrets
 ```bash
kubectl create secret generic ts-ems-secrets --from-literal=EMS_PASSWORD=titkos123
kubectl get secret/ts-ems-secrets
kubectl describe secret/ts-ems-secrets
kubectl apply -f ts-ems-deployment-2.yaml
 ```