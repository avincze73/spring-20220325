package hu.sprint.ts.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.ts.ems.*"})
@EntityScan(basePackages = {"hu.sprint.ts.ems"})
@EnableJpaRepositories(basePackages = {"hu.sprint.ts.ems.repository"})
@EnableRedisHttpSession
public class TsEmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsEmsApplication.class, args);
	}

}
