# Basics of Kubernetes

## Building the application
```bash
curl https://start.spring.io/starter.tgz -d dependencies=web -d baseDir=simple-microservice \
 -d bootVersion=2.5.5 -d javaVersion=111 | tar -xzvf -


curl https://start.spring.io/starter.tgz -d dependencies=web -d baseDir=simple-microservice \
 -d bootVersion=2.5.5 -d javaVersion=11 | tar -xzvf -
 
curl https://start.spring.io/starter.tgz -d dependencies=web,jpa \
 -d baseDir=boot-demo -d bootVersion=2.5.5 -d javaVersion=11 \
 -d groupId=hu.sprint -d artifactId=boot-demo \
  -d name=boot-demo \
 -d packageName=hu.sprint.spring.boot.demo | tar -xzvf -
 
 
 az monitor log-analytics workspace create -g SprintResourceGroup -n SprintWorskpace
  
```

## Actuator 

``` bash
HATEOAS
curl http://localhost:8080/actuator


curl http://localhost:8080/actuator/health
curl http://localhost:8080/actuator/info
curl http://localhost:8080/actuator/metrics
curl http://localhost:8080/actuator/metrics/jvm.gc.pause


curl http://localhost:8080/actuator/configprops
curl http://localhost:8080/actuator/httptrace
curl http://localhost:8080/actuator/mappings
curl http://localhost:8080/actuator/scheduledtasks
curl http://localhost:8080/actuator/metrics
curl http://localhost:8080/actuator/shutdown
curl http://localhost:8080/actuator/configprops


curl -X POST \
  http://localhost:8080/actuator/shutdown\
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4b337573-ed7c-48f4-b281-c8c53f47dde8' \
  -H 'cache-control: no-cache' 
  


curl -X POST \
  http://localhost:8080/actuator/custom-rest-endpoint \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4b337573-ed7c-48f4-b281-c8c53f47dde8' \
  -H 'cache-control: no-cache' \
  -d '{
"request":"This is my request"
}'


curl -X GET \
  http://localhost:8080/actuator/custom-endpoint \
  -H 'Postman-Token: d26ab39f-36bf-4165-b00c-855b2dbfca9d' \
  -H 'cache-control: no-cache'




curl -X POST 'http://localhost:8080/realms/SpringBootKeycloak/protocol/openid-connect/token' \
 --header 'Content-Type: application/x-www-form-urlencoded' \
 --data-urlencode 'grant_type=password' \
 --data-urlencode 'client_id=springboot-microservice' \
 --data-urlencode 'username=admin' \
 --data-urlencode 'password=admin'
 
 
```



[Főoldal](../README.md)