package hu.cib.conditionalonproperty.service;

public interface NotificationSender {
    String send(String message);
}
