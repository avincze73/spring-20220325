package hu.cib.conditionalonproperty;

import static org.assertj.core.api.Assertions.assertThat;

import hu.cib.conditionalonproperty.config.NotificationConfig;
import hu.cib.conditionalonproperty.service.EmailNotification;
import hu.cib.conditionalonproperty.service.NotificationSender;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;


public class NotificationUnitTest {

    private final ApplicationContextRunner contextRunner = new ApplicationContextRunner();

    @Test
    public void whenValueSetToEmail_thenCreateEmailNotification() {
        this.contextRunner.withPropertyValues("notification.service=email")
            .withUserConfiguration(NotificationConfig.class)
            .run(context -> {
                assertThat(context).hasBean("emailNotification");
                NotificationSender notificationSender = context.getBean(EmailNotification.class);
                assertThat(notificationSender.send("Hello From CIB!")).isEqualTo("Email Notification: Hello From CIB!");
                assertThat(context).doesNotHaveBean("smsNotification");
            });
    }
}
