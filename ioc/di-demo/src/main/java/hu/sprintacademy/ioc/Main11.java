package hu.sprintacademy.ioc;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main11 {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        //UserService userService = (UserService) context.getBean("userServiceWithInit2", UserServiceImplWithInit2.class);

        //System.out.println(userService.getId());

        context.close();


    }
}
