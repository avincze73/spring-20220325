package hu.sprintacademy.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main9 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");


		EmployeeWithInheritance employee6 = (EmployeeWithInheritance) context.getBean("employee7");
		System.out.println(employee6);
		

		
		((ConfigurableApplicationContext)context).close();
		
		
	}
}
