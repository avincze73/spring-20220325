package hu.sprintacademy.ioc;

public class UserServiceImplWithInit2 implements UserService {
	private UserDao userDao;

	public UserServiceImplWithInit2() {
		super();
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public Long getId() {
		return System.currentTimeMillis();
	}

	public void myInit() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Employee11 myInit... ");

	}

	public void myDestroy() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Employee11 myDestroy... ");

	}

}
