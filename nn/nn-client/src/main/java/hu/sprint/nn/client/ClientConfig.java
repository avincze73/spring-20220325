package hu.sprint.nn.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ClientConfig {

    @Value( "${nn.rest.url}" )
    private String nnRestUrl;


    @Bean
    public WebClient webClient() {
        return WebClient.builder()
                .baseUrl(nnRestUrl)
                .build();
    }
}
