package hu.sprint.nn.client;

import hu.sprint.nn.dm.entity.Department;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "nnrest", url = "${NN_REST_URL:http://localhost}:8001/nn-rest")
public interface NnRestProxy {
    @GetMapping("/api/departments")
    ResponseEntity<List<Department>> getDepartments();

    @PostMapping("/api/departments")
    Department createDepartment(@RequestBody @Validated Department department);
}
