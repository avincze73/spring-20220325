package hu.sprint.nn.client;

import hu.sprint.nn.dm.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DeadlockLoserDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@RestController
public class ClientController {

    @Value( "${nn.rest.url}" )
    private String nnRestUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private NnRestProxy nnRestProxy;

    @Autowired
    private WebClient webClient;

    @GetMapping("/1/api/departments")
    public ResponseEntity<GetDepartmentsResponse> getDepartments(){
        GetDepartmentsResponse result = new GetDepartmentsResponse();
        ResponseEntity<Department[]> response
                = restTemplate.getForEntity(nnRestUrl + "/api/departments", Department[].class);
        result.getDepartmentList().addAll(Arrays.asList(response.getBody()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/2/api/departments")
    public ResponseEntity<GetDepartmentsResponse> getDepartments2(){
        GetDepartmentsResponse result = new GetDepartmentsResponse();
//        WebClient departmentClient =  WebClient.builder()
//                .baseUrl(nnRestUrl)
//                .build();
        Mono<Department[]> response = webClient.get().uri("/api/departments")
                .retrieve()
                .bodyToMono(Department[].class);
        result.getDepartmentList().addAll(Arrays.asList(response.block()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/3/api/departments")
    public ResponseEntity<GetDepartmentsResponse> getDepartments3(){
        GetDepartmentsResponse result = new GetDepartmentsResponse();
        result.getDepartmentList().addAll(nnRestProxy.getDepartments().getBody());
        return ResponseEntity.ok(result);
    }


    @GetMapping("/admin/api/departments")
    public ResponseEntity<GetDepartmentsResponse> getAdminDepartments(){
        GetDepartmentsResponse result = new GetDepartmentsResponse();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("admin");

        LoginResponse loginResponse =
                webClient.post().uri("/login")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .body(Mono.just(loginRequest), LoginRequest.class)
                        .retrieve()
                        .bodyToMono(LoginResponse.class).block();

        Mono<Department[]> response = webClient.get().uri("/admin/departments")
                .headers(h -> h.setBearerAuth(loginResponse.getToken()))
                .retrieve()
                .bodyToMono(Department[].class);
        result.getDepartmentList().addAll(Arrays.asList(response.block()));
        return ResponseEntity.ok(result);
    }


    @PostMapping("/1/api/departments")
    @ResponseStatus(HttpStatus.CREATED)
    public PostDepartmentsResponse createDepartment(@RequestBody @Validated Department department) {
        PostDepartmentsResponse result = new PostDepartmentsResponse();
        ResponseEntity<Department> response
                = restTemplate.postForEntity(nnRestUrl + "/api/departments", department, Department.class);
        result.setDepartment(response.getBody());
        return result;
    }

    @PostMapping("/2/api/departments")
    @ResponseStatus(HttpStatus.CREATED)
    public PostDepartmentsResponse createDepartment2(@RequestBody @Validated Department department) {
        PostDepartmentsResponse result = new PostDepartmentsResponse();
//        WebClient departmentClient =  WebClient.builder()
//                .baseUrl(nnRestUrl)
//                .build();
        Department createdDepartment = webClient.post()
                .uri("/api/departments")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(department), Department.class)
                .retrieve()
                .bodyToMono(Department.class).block();
        result.setDepartment(createdDepartment);
        return result;
    }

    @PostMapping("/3/api/departments")
    @ResponseStatus(HttpStatus.CREATED)
    public PostDepartmentsResponse createDepartment3(@RequestBody @Validated Department department) {
        PostDepartmentsResponse result = new PostDepartmentsResponse();
        result.setDepartment(nnRestProxy.createDepartment(department));
        return result;
    }

    @PostMapping("/admin/api/departments")
    @ResponseStatus(HttpStatus.CREATED)
    public PostDepartmentsResponse createAdminDepartment(@RequestBody @Validated Department department) {
        PostDepartmentsResponse result = new PostDepartmentsResponse();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("admin");

        LoginResponse loginResponse =
                webClient.post().uri("/login")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .body(Mono.just(loginRequest), LoginRequest.class)
                        .retrieve()
                        .bodyToMono(LoginResponse.class).block();

        Department createdDepartment = webClient.post()
                .uri("/admin/departments")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .headers(h -> h.setBearerAuth(loginResponse.getToken()))
                .body(Mono.just(department), Department.class)
                .retrieve()
                .bodyToMono(Department.class).block();
        result.setDepartment(createdDepartment);
        return result;
    }

    @GetMapping("/to-read")
    public Mono<String> toRead() {
        return WebClient.builder().baseUrl(nnRestUrl).build()
                .get().uri("/recommended").retrieve()
                .bodyToMono(String.class);
    }

}
