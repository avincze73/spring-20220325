package hu.sprint.nn.dm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class DepartmentRegistrationException extends RuntimeException {
    public DepartmentRegistrationException(String message) {
        super(message);
    }
}
