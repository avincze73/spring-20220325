package hu.sprint.nn.dm.entity;



import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

@Entity
public class Department extends NNEntity {

	@Basic(optional = false)
	@Column(name = "name")
	@NotEmpty(message = "Name should not be empty")
	private String name;

	@Embedded
	private Address address;

	public Department() {
		// TODO Auto-generated constructor stub
		this(null, null);
	}

	public Department(String name, Address address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Department(String name) {
		// TODO Auto-generated constructor stub
		this(name, null);
	}

	public Department(Department department){
		this.setId(department.getId());
		this.setAddress(department.getAddress());
		this.setName(department.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
