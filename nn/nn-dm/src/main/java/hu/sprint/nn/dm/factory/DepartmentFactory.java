package hu.sprint.nn.dm.factory;

import hu.sprint.nn.dm.entity.Address;
import hu.sprint.nn.dm.entity.Department;

public class DepartmentFactory {

    //Facade
    public static Department create(String name, String zip,
                                    String street, String city){
        return  new Department(name, new Address(zip, city, street));
    }
}
