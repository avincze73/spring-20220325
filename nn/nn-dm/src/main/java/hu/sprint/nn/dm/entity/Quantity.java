package hu.sprint.nn.dm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class Quantity {
    @Column
    private double value;

    @Column
    private String unit;
}
