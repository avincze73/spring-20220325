package hu.sprint.nn.dm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "Recipes")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long recipeId;

    @Column
    private String name;

    @ElementCollection(targetClass = Ingredient.class)
    @JoinTable(name = "Ingredients")
    @JoinColumn(name = "recipeId", referencedColumnName = "recipeId")
    private Set<Ingredient> ingredients;
    public void addIngredient(Ingredient ingredient)
    {
        this.ingredients.add(ingredient);
    }
}
