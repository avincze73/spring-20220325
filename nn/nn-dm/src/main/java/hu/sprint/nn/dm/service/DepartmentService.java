package hu.sprint.nn.dm.service;

import hu.sprint.nn.dm.entity.Address;
import hu.sprint.nn.dm.entity.Department;
import hu.sprint.nn.dm.exception.DepartmentRegistrationException;
import hu.sprint.nn.dm.factory.DepartmentFactory;
import hu.sprint.nn.dm.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;


    public Optional<Department> getByName(String name) {
        return departmentRepository.findByName(name);
    }

    @Transactional
    public Department save(Department department) {
        return departmentRepository.save(department);
    }


    @Transactional
    public Department save(String name, Address address) {
        return departmentRepository.save(new Department(name, address));
    }

    @Transactional
    public Department save(String name, String zip, String street, String city) {
        Department department = DepartmentFactory.create(name, zip, street, city);
        Optional<Department> departmentOptional = departmentRepository.findByName(name);
        if (departmentOptional.isPresent()){
            throw new DepartmentRegistrationException("Department already exists");
        }
        return departmentRepository.save(department);
    }



    public Department update(Department department) {
        return departmentRepository.save(department);
    }

    public List<Department> getAll() {
        return departmentRepository.findAll();
    }

    public Optional<Department> getById(Long id) {
        return departmentRepository.findById(id);
    }

    public void deleteById(Long id){
        departmentRepository.deleteById(id);
    }




}
