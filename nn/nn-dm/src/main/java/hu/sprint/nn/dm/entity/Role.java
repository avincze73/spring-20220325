package hu.sprint.nn.dm.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Role extends NNEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	
	public Role() {
		// TODO Auto-generated constructor stub
		this(null);
	}
	
	public Role(String name) {
		super();
		this.name = name;
	}
	
}
