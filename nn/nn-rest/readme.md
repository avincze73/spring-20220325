# Creating docker image
```bash
mvn clean package
docker-compose -f docker-compose-basic.yaml build
docker-compose -f docker-compose-layered.yaml build

jar tf target/nn-rest-0.0.1-SNAPSHOT.jar
java -Djarmode=layertools -jar target/nn-rest-0.0.1-SNAPSHOT.jar list
java -Djarmode=layertools -jar target/nn-rest-0.0.1-SNAPSHOT.jar extract
```

# Pushing the image to azure container registry
```bash
az acr check-health -n SprintContainerRegistry2021 --yes
az acr login --name SprintContainerRegistry2021
az acr show --name SprintContainerRegistry2021 --query loginServer
az acr login --name sprintcontainerregistry2021 --expose-token --output tsv --query accessToken
#Use the token in order to login
docker login sprintcontainerregistry2021.azurecr.io -u 00000000-0000-0000-0000-000000000000 -p \
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVWVzc6T1JDQjozVkNIOjY2UEI6R1kyWjpOU01KOldIVVE6Qjc3WDpLWTdaOlRaU1A6UkZZWDpIVTdRIn0.eyJqdGkiOiI1ZjUyMGE4Yy00NGE0LTRhYzItODU5OC1jYjEzNzJjZTVjNjQiLCJzdWIiOiJhZG1pbkBzcHJpbnRhY2FkZW15Lm9ubWljcm9zb2Z0LmNvbSIsIm5iZiI6MTYzOTY0NjY3OCwiZXhwIjoxNjM5NjU4Mzc4LCJpYXQiOjE2Mzk2NDY2NzgsImlzcyI6IkF6dXJlIENvbnRhaW5lciBSZWdpc3RyeSIsImF1ZCI6InNwcmludGNvbnRhaW5lcnJlZ2lzdHJ5MjAyMS5henVyZWNyLmlvIiwidmVyc2lvbiI6IjEuMCIsInJpZCI6ImQ2NGE2OGQ1NzMxNTQ4M2Q5NWQ1ODEyN2I0Mzg1NGQ3IiwiZ3JhbnRfdHlwZSI6InJlZnJlc2hfdG9rZW4iLCJhcHBpZCI6IjA0YjA3Nzk1LThkZGItNDYxYS1iYmVlLTAyZjllMWJmN2I0NiIsInRlbmFudCI6ImUyYTM2ZjZhLTk1MTYtNDgxZi04Yjg4LWQxMDUzYjFiMDI5YyIsInBlcm1pc3Npb25zIjp7IkFjdGlvbnMiOlsicmVhZCIsIndyaXRlIiwiZGVsZXRlIl0sIk5vdEFjdGlvbnMiOm51bGx9LCJyb2xlcyI6W119.ZF0Ei-L-IY-ivtPFAClhD7euFmyKnooBAeY00LsaTyQKc7CYcIhGxcjwCuVqe9GfPVtNPg9GJfJ90fbzZxAaT5OTtfmWYl4SL36NWIeC609xIsHVW0adbkdNiX30kVqj-wt1L2o5J83MrljeXP4bvc_jkfsncf30jkBRL0MBmFo3Z7IJRTn1AIRvhrZkk7b6Z507p8Gz9DbvtKWSl-Qy0nvY0ewRCJp3BzYbA8DrMLpgvLwT8BmbWLu4OZkgG7OaKRcKPrnDUCoIzxHMkqvD438jY-q9-fmc7g5IYRIsBJd86iQJiY3QXWv7NStN3-jfRgsgkV910SF39TFoM2PWgw

docker login sprintcontainerregistry2021.azurecr.io --username 890c472c-c2b7-4d86-830f-be6c6dd3b0c0 --password gWqLDVcP3b8Jp9~eWoTq_hkD4BV5oWH0t9


docker tag avincze73/nn-rest:layered sprintcontainerregistry2021.azurecr.io/nn-rest:layered
docker push sprintcontainerregistry2021.azurecr.io/nn-rest:layered

az acr repository list --name sprintcontainerregistry2021.azurecr.io --output tsv
az acr repository show-tags --name sprintcontainerregistry2021.azurecr.io --repository nn-rest --output tsv
```

# Creating container instance
```bash
az acr show --name sprintcontainerregistry2021 --query id --output tsv
# /subscriptions/a1c3e388-a1d4-4a7a-8f14-75f2d95b62a5/resourceGroups/SprintResourceGroup/providers/Microsoft.ContainerRegistry/registries/SprintContainerRegistry2021
az ad sp create-for-rbac --name http://sprintcontainerregistry2021b-service-principal --scopes /subscriptions/a1c3e388-a1d4-4a7a-8f14-75f2d95b62a5/resourceGroups/SprintResourceGroup/providers/Microsoft.ContainerRegistry/registries/SprintContainerRegistry2021 --role acrpull --query  password   --output tsv
# gWqLDVcP3b8Jp9~eWoTq_hkD4BV5oWH0t9
az ad sp list --display-name http://sprintcontainerregistry2021b-service-principal --query "[].appId" -o tsv
# 890c472c-c2b7-4d86-830f-be6c6dd3b0c0

az acr credential show -n sprintcontainerregistry2021 --query username
az acr credential show -n sprintcontainerregistry2021 --query passwords


az acr credential show -n sprintcontainerregistry2021 --query username
az acr credential show -n sprintcontainerregistry2021 --query passwords


az container create --resource-group SprintResourceGroup --name nn-rest-va --image sprintcontainerregistry2021.azurecr.io/nn-rest:layered \
--cpu 1 --memory 1 --registry-login-server sprintcontainerregistry2021.azurecr.io --registry-username SprintContainerRegistry2021 \
--registry-password /=JfMR9tyXF8lWcy8m8wrZ=i8oxo8D71 --dns-name-label nn-rest-va --ports 8001

az container logs --resource-group SprintResourceGroup --name nn-rest-va 

http://nn-rest-va.westeurope.azurecontainer.io:8001/nn-rest/swagger-ui/index.html?configUrl=/nn-rest/v3/api-docs/swagger-config


az container delete --resource-group SprintResourceGroup --name nn-rest-va 
```


# Creating aks secret
```bash
kubectl create secret docker-registry sprintcontainerregistry2021secret2 --docker-server=http://sprintcontainerregistry2021.azurecr.io --docker-username=890c472c-c2b7-4d86-830f-be6c6dd3b0c0 --docker-password=gWqLDVcP3b8Jp9~eWoTq_hkD4BV5oWH0t9 --docker-email=avincze73@gmail.com

az aks get-credentials --resource-group SprintResourceGroup --name akssprint2021
```

# Creating azure keyvault
az ad sp create-for-rbac --name sprintspkv2 --role Contributor
```bash
#{
#  "appId": "5000d13b-5a35-4389-989a-0db20cc4d8c8",
#  "displayName": "sprintspkv2",
#  "name": "5000d13b-5a35-4389-989a-0db20cc4d8c8",
#  "password": "iuHC8dlKG2Vda.EuN5LJ9ulVLX6LPFwDDX",
#  "tenant": "e2a36f6a-9516-481f-8b88-d1053b1b029c"
#}


az keyvault create \
    --resource-group SprintResourceGroup \
    --name sprintmysqlkv \
    --enabled-for-deployment true \
    --enabled-for-disk-encryption true \
    --enabled-for-template-deployment true \
    --location westeurope \
    --query properties.vaultUri \
    --sku standard
# "https://sprintmysqlkv.vault.azure.net/"
    
az keyvault set-policy --name sprintmysqlkv --spn 5000d13b-5a35-4389-989a-0db20cc4d8c8 --secret-permissions get list

az keyvault secret set --name "spring-datasource-password" --vault-name "sprintmysqlkv" --value "titkos123"

    
```



```bash
curl -i \ 
-H "Accept: application/json" \ 
-H "Content-Type:application/json" \ 
-X POST --data  '{"username": "johnny", "password": "password"}' "https://localhost:8001/nn-rest/content"
   
curl -H "Content-Type: application/json" -H "Accept: application/json"\
    --request POST \
    --data '{"username": "johnny", "password": "password"}' \
    http://localhost:8001/nn-rest/content | jq 
    
curl -H "Content-Type: application/json" -H "Accept: application/xml"\
    --request POST \
    --data '{"username": "johnny", "password": "password"}' \
    http://localhost:8001/nn-rest/content  | jq   
    
curl --request GET \
    http://localhost:8001/nn-rest/api/departments       
    
curl -H "accept-language: eng" \
    --request GET \
    http://localhost:8001/nn-rest/greeting 
    
           
```



# HATEOAS (Hypertext as the Engine of Application State)
```bash

```