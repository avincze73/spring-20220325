package hu.sprint.nn.rest;

import hu.sprint.nn.dm.entity.Department;
import hu.sprint.nn.dm.exception.DepartmentRegistrationException;
import hu.sprint.nn.dm.factory.DepartmentFactory;
import hu.sprint.nn.dm.repository.DepartmentRepository;
import hu.sprint.nn.dm.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = DepartmentService.class)
public class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepository;

    @Test
    public void shouldSaveDepartment(){
        Department department = DepartmentFactory.create("name1", "1111", "street", "city");
        BDDMockito.given(departmentRepository.findByName("name1")).willReturn(Optional.empty());
        BDDMockito.given(departmentRepository.save(department)).willAnswer(invocation -> invocation.getArgument(0));

        Department saved = departmentService.save("name1", "1111", "street", "city");
        assertThat(saved).isNotNull();
        BDDMockito.then(departmentRepository).should(times(1)).save(any(Department.class));
    }

    @Test
    void shouldThrowErrorWhenSaveDepartmentWithExistingName() {
        Department department = DepartmentFactory.create("name1", "1111", "street", "city");

        given(departmentRepository.findByName(department.getName())).willReturn(Optional.of(department));

        assertThrows(DepartmentRegistrationException.class,() -> {
            departmentService.save("name1", "1111", "street", "city");
        });

        then(departmentRepository).should(never()).save(any(Department.class));
        then(departmentRepository).should(times(1)).findByName(any(String.class));
    }


    @Test
    void shouldUpdateUser() {
        Department department = DepartmentFactory.create("name1", "1111", "street", "city");
        given(departmentRepository.save(department)).willReturn(department);

        Department expected = departmentService.update(department);

        assertThat(expected).isNotNull();

        then(departmentRepository).should(times(1)).save(any(Department.class));
    }

    @Test
    void shouldReturnFindAll() {
        List<Department> datas = new ArrayList();
        datas.add(DepartmentFactory.create("name1", "1111", "street", "city"));
        datas.add(DepartmentFactory.create("name2", "1111", "street", "city"));
        datas.add(DepartmentFactory.create("name3", "1111", "street", "city"));

        given(departmentRepository.findAll()).willReturn(datas);

        List<Department> expected = departmentService.getAll();

        assertEquals(expected, datas);
        then(departmentRepository).should(times(1)).findAll();
    }

    @Test
    void shouldFindDepartmentById(){
        long id = 1;
        Department department = DepartmentFactory.create("name1", "1111", "street", "city");

        given(departmentRepository.findById(id)).willReturn(Optional.of(department));

        Optional<Department> expected = departmentService.getById(id);

        assertThat(expected.get()).isNotNull();
        then(departmentRepository).should(times(1)).findById(id);
    }

    @Test
    void shouldDelete() {
        long id =1;

        departmentService.deleteById(id);
        departmentService.deleteById(id);

        then(departmentRepository).should(times(2)).deleteById(id);
    }


}
