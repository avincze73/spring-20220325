package hu.sprint.nn.rest;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class User {

    private long id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotEmpty(message = "{email.notempty}")
    private String email;

    @NotEmpty(message = "{notempty.user.title}")
    private String title;


}
