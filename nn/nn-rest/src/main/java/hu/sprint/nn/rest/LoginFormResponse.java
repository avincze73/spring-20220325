package hu.sprint.nn.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@JacksonXmlRootElement
public class LoginFormResponse {
    @JsonFormat()
    private String text;
}
