package hu.sprint.nn.rest;

import hu.sprint.nn.dm.entity.Department;
import hu.sprint.nn.dm.service.DepartmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = {"/api/departments", "/user/departments", "/admin/departments"})
public class DepartmentRestController {

    @Autowired
    private DepartmentService departmentService;


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Department> getAllDepartments() {
        return departmentService.getAll();
    }

    @Operation(summary = "Get a Department by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Department found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Department.class)) }),
//            @ApiResponse(responseCode = "404", description = "Department not found - 1",
//                    content = { @Content(mediaType = "application/json",
//                            schema = @Schema(implementation = LoginRequest.class)) }),
            @ApiResponse(responseCode = "400", description = "Department not found - 2",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(anyOf = {LoginResponse.class, LoginRequest.class})) }) }
    )
    @GetMapping("/{id}")
    public ResponseEntity<?> getDepartmentsById(@PathVariable Long id) {
        Optional<Department> result = departmentService.getById(id);
        if (result.isPresent()){
            return ResponseEntity.ok(result.get());
        } else {
            return new ResponseEntity<LoginRequest>(new LoginRequest("a", "b"), HttpStatus.NOT_FOUND);
        }
//        return departmentService.getById(id)
//                .map( department -> ResponseEntity.ok(department))
//                .orElseGet(() -> new ResponseEntity<LoginRequest>(new LoginRequest("a", "b"), HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Department createDepartment(@RequestBody @Validated Department departmentDTO) {
        return departmentService.save(departmentDTO);
    }


    @PostMapping("/exception")
    @ResponseStatus(HttpStatus.CREATED)
    public Department createDepartmentWithException(@RequestBody @Validated Department departmentDTO) {
        return departmentService.save(departmentDTO.getName(), departmentDTO.getAddress().getZip(), departmentDTO.getAddress().getCity(), departmentDTO.getAddress().getStreet());
    }


    @PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable Long id, @RequestBody Department department) {
        return departmentService.getById(id)
                .map(departmentObj -> {
                    departmentObj.setId(id);
                    return ResponseEntity.ok(departmentService.update(departmentObj));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Department> deleteDepartment(@PathVariable Long id) {
        return departmentService.getById(id)
                .map(department -> {
                    departmentService.deleteById(id);
                    return ResponseEntity.ok(department);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
