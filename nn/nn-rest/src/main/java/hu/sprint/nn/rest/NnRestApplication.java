package hu.sprint.nn.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan(basePackages = {"hu.sprint.nn.dm.entity"})
@EnableJpaRepositories(basePackages = {"hu.sprint.nn.dm.repository", "hu.sprint.nn.rest"})
@ComponentScan(basePackages = {"hu.sprint.nn.dm.*", "hu.sprint.nn.rest", "hu.sprint.nn.rest.*"})
@EnableTransactionManagement
public class NnRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(NnRestApplication.class, args);
	}

}
