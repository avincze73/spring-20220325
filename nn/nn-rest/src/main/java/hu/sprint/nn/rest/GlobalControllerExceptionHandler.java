package hu.sprint.nn.rest;

import hu.sprint.nn.dm.exception.DepartmentRegistrationException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    //@ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<String> handleConnversion(RuntimeException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler( DepartmentRegistrationException.class )
    protected ResponseEntity<Object> handleConflict( RuntimeException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_ACCEPTABLE);
    }


//    @ExceptionHandler( MethodArgumentNotValidException.class )
//    protected ResponseEntity<Object> handleConflict2( Exception ex) {
//        return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
//    }

}
