package hu.sprint.nn.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginFormRequest {
    private String username;
    private String password;
}
