# SOAP

## Setup 
```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,web-services \
-d baseDir=soap-simple -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=soap-simple \
-d name=soap-simple \
-d packageName=hu.cib.soap.simple | tar -xzvf -
```


```bash
curl https://start.spring.io/starter.tgz -d dependencies=web,web-services \
-d baseDir=soap-client -d bootVersion=2.5.5 -d javaVersion=11 \
-d groupId=hu.cib -d artifactId=soap-client \
-d name=soap-client \
-d packageName=hu.cib.soap.client | tar -xzvf -
```
Spring-WS only supports the contract-first development style.


[Főoldal](../README.md)