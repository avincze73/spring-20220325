package hu.sprintacademy.cassandra;

import hu.sprintacademy.cassandra.model.Tutorial;
import hu.sprintacademy.cassandra.repository.TutorialRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.cassandra.DataCassandraTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@DataCassandraTest
class SpringBootDataCassandraApplicationTests {


	@Autowired
	private TutorialRepository tutorialRepository;

	@Test
	void contextLoads() {
	}

	@BeforeEach
	public void init(){
		tutorialRepository.deleteAll();
	}

	@Test
	@Rollback(value = true)
	void givenValidCarRecord_whenSavingIt_thenRecordIsSaved() {
		Tutorial entity = new Tutorial(UUID.randomUUID(), "Tutorial title", "Tutorial description", false);

		tutorialRepository.save(entity);

		List<Tutorial> saved = tutorialRepository.findAll();
		assertThat(saved.get(0).getTitle()).isEqualTo(entity.getTitle());
	}



}
