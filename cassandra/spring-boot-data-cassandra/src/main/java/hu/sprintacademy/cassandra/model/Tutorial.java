package hu.sprintacademy.cassandra.model;

import java.util.UUID;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Tutorial {
  @PrimaryKey
  private UUID id;

  private String title;
  private String description;
  private boolean published;

}
