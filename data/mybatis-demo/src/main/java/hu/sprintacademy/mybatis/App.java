package hu.sprintacademy.mybatis;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(MyConfiguration.class);

        ArticleRepository articleRepository =
                applicationContext.getBean(ArticleRepository.class);
        System.out.println(applicationContext.hashCode());
        System.out.println(articleRepository.hashCode());

        articleRepository =
                applicationContext.getBean(ArticleRepository.class);
        System.out.println(articleRepository.hashCode());



        applicationContext =
                new AnnotationConfigApplicationContext(MyConfiguration.class);
        System.out.println(applicationContext.hashCode());

        articleRepository =
                applicationContext.getBean(ArticleRepository.class);
        System.out.println(articleRepository.hashCode());

        articleRepository =
                applicationContext.getBean(ArticleRepository.class);
        System.out.println(articleRepository.hashCode());

        articleRepository.findAll().forEach(System.out::println);

    }
}
