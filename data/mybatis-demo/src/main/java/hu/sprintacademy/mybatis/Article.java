package hu.sprintacademy.mybatis;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Article {
    private Long id;
    private String title;
    private String author;
}
