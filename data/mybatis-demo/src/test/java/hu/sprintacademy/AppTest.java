package hu.sprintacademy;

import hu.sprintacademy.mybatis.Article;
import hu.sprintacademy.mybatis.ArticleRepository;
import hu.sprintacademy.mybatis.MyConfiguration;
//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//Spring context is not reachable
@ContextConfiguration(classes = MyConfiguration.class)
public class AppTest 
{
    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void shouldAnswerWithTrue()
    {
       assertTrue( true );
    }

    @Test
    public void whenRecordsInDatabase_shouldReturnArticleWithGivenId() {
        Article article = articleRepository.findById(1L);

        assertThat(article).isNotNull();
        assertThat(article.getId()).isEqualTo(1L);
        assertThat(article.getAuthor()).isEqualTo("IQJB");
        assertThat(article.getTitle()).isEqualTo("Working with MyBatis in Spring");
    }
}
