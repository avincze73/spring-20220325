# Spring data with mongodb

## Installing mongodb

``` shell script
brew tap mongodb/brew
brew install mongodb-community
brew services start mongodb-community
brew services stop mongodb-community

docker run --name my-first-mongo -p 27017:27017 mongo

```

The install creates:

- the configuration file (/usr/local/etc/mongod.conf)
- the log directory path (/usr/local/var/log/mongodb)
- the data directory path (/usr/local/var/mongodb)

Connect and Use MongoDB
- default port 27017
``` shell script
mongo
```