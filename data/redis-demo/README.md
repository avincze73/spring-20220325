# Installing redis

``` shell script
brew install redis
redis-cli ping
brew services start redis
brew services stop redis

docker run --name my-first-redis -p 6379:6379 redis
```