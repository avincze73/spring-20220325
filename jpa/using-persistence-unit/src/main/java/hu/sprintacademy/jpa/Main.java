package hu.sprintacademy.jpa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(MyConfiguration.class);

        StudentDaoJpaImpl dao = (StudentDaoJpaImpl) applicationContext.getBean("studentDaoJpaImpl");

        Student student = new Student();
        student.setFirstName("Attila1");
        student.setLastName("Vincze1");

        dao.save(student);
    }

}