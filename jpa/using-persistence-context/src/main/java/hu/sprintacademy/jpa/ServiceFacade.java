package hu.sprintacademy.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;


@Service
public class ServiceFacade {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private DepartmentDao departmentDao;

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void setDepartmentDao(DepartmentDao departmentDao) {
        this.departmentDao = departmentDao;
    }

    @Transactional
    public void save(Book book, Department department) throws Exception {
        //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();


        bookDao.save(book);

         if ("book22".equals(book.getName())) {
            //throw new MyException3();
            //throw new RuntimeException("rt");
        }
        departmentDao.save(department);

//        if(!TransactionAspectSupport.currentTransactionStatus().isRollbackOnly()) {
//            departmentDao.save(department);
//        }

    }

    @Transactional(rollbackFor = {MyException.class}, noRollbackFor = {MyException3.class})
    public void save2(Book book, Department department) throws Exception  {
        bookDao.save(book);
        if ("book22".equals(book.getName())){
            throw new RuntimeException();
        }
        departmentDao.save(department);
    }

}
