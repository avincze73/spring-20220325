package hu.sprintacademy.jpa;

public interface DepartmentDao {
    public void save(Department department);
}
