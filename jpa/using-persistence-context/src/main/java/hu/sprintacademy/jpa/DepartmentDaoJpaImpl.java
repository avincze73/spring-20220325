package hu.sprintacademy.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class DepartmentDaoJpaImpl implements DepartmentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Department department) {
        entityManager.persist(department);
    }
}
