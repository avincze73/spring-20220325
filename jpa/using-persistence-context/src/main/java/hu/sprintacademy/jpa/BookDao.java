package hu.sprintacademy.jpa;

public interface BookDao {
    public void save(Book book);
}
