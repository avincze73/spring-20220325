package hu.sprintacademy.jpa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(MyConfiguration.class);

        BookService bookService
                = applicationContext
                .getBean(BookService.class);

        Book book = new Book();
        book.setName("book1");
        //bookService.save(book);



        BookDao bookDao = applicationContext.getBean(BookDao.class);
        //bookDao.save(book);



        ServiceFacade serviceFacade = applicationContext
                .getBean(ServiceFacade.class);
        book = new Book();
        book.setName("book22");
        Department department = new Department();
        department.setName("department22");

        try {
            serviceFacade.save(book, department);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
