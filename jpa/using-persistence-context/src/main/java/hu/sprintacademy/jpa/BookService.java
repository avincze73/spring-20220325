package hu.sprintacademy.jpa;

public interface BookService {
    public void save(Book book);
}
