package hu.sprintacademy.jpa;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

public class StudentDaoJpaImpl {

    @PersistenceUnit
    //@Autowired
    private EntityManagerFactory entityManagerFactory;

    public void save(Student student) {
        EntityManager entityManager = entityManagerFactory
                .createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(student);
        transaction.commit();
        entityManager.close();
    }
}
