package com.example.rabbitmq.app;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
@Profile({"main"})
public class QueueConsumer {

    @RabbitListener(queues = {"${queue.name}"})
    public void receive(@Payload Message message) throws BusinessException {
        System.out.println("Receiver1 " + message + "  " + LocalDateTime.now());
        String academy = String.valueOf(message.getHeaders().get("sprint"));
        if(academy.equals("academy")){
            System.out.println(academy);
        }
        String payload = String.valueOf(message.getPayload());
        if(payload.equals(1)) {
            throw new BusinessException("rabbit error");
        }
    }


    @RabbitListener(queues = {"${queue.name}"})
    public void receive2(@Payload String message) throws BusinessException {
        System.out.println("Receiver2 " + message );

    }

}