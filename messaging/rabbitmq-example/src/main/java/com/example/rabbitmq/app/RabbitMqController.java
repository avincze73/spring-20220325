package com.example.rabbitmq.app;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("main")
public class RabbitMqController {

    public RabbitMqController(RabbitTemplate queueSender) {
        this.queueSender = queueSender;
    }

    private final RabbitTemplate queueSender;

    @Autowired
    private QueueSender queueSender1;

    @GetMapping("/rabbitmq1")
    public String send1(){

        String welcome = "Welcome to Sprint Academy";
        queueSender1.send(welcome);
        return "done";
    }

    @GetMapping("/rabbitmq2")
    public String send2(){

        String welcome = "Welcome to Sprint Academy";

        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("sprint", "academy");
        Message message = new Message(welcome.getBytes(), messageProperties);

        queueSender.convertAndSend("direct-exchange", "test-routing-key", message);
        return "done";
    }

}
