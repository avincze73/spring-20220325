/*
 * Copyright 2015-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.rabbitmq.app.demo2;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Profile({"demo2"})
@Configuration
public class Demo2Config {

    @Bean
    public Queue hello() {
        return new Queue("vinczeattila");
    }

    @Bean
    @Profile("demo2")
    public Demo2Receiver receiver1() {
        return new Demo2Receiver(1);
    }

    @Bean
    @Profile("demo2")
    public Demo2Receiver receiver2() {
        return new Demo2Receiver(2);
    }

    @Profile("demo2")
    @Bean
    public Demo2Sender sender() {
        return new Demo2Sender();
    }

}
