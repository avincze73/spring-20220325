package hu.sprintacademy.introduction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        GreetingService greetingService =
                (GreetingService) context.getBean("greetingServiceImpl1");
        System.out.println(greetingService.greeting());
        ((ConfigurableApplicationContext) context).close();
    }

}
