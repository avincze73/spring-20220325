package hu.sprintacademy.introduction;

public interface GreetingService {
    String greeting();
}
