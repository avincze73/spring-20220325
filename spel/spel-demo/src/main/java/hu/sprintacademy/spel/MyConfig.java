package hu.sprintacademy.spel;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "hu.sprintacademy.spel")
public class MyConfig {


}
